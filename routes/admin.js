var express = require('express');
var router = express.Router();
var { isadminLoggedIn } = require("../utility/adminAuthToken");

const {
  adminLoginRoute,
  adminChangePasswordRoute
} = require("../controller/adminController");

router.post('/login', adminLoginRoute);
router.post('/changePassword', isadminLoggedIn, adminChangePasswordRoute);

module.exports = router;
