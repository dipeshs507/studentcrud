var express = require('express');
var router = express.Router();
const multer = require('multer')
var { isLoggedIn } = require("../utility/userAuthToken");
var uploadImage = require("../utility/uploadImage");

const {
  userRegisterRoute,
  userLoginRoute,
  userProfileRoute, 
  editProfileRoute,
  deleteUser
} = require("../controller/userController");

router.post('/userRegister', userRegisterRoute);
router.post('/userLogin', userLoginRoute);
router.get('/userProfile', isLoggedIn, userProfileRoute);
router.post('/editProfile', isLoggedIn, uploadImage.single("profileImage"), editProfileRoute);
router.post('/delete', isLoggedIn, deleteUser);

module.exports = router;

