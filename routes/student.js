var express = require('express');
var router = express.Router();
const multer = require('multer')
var uploadImage = require("../utility/uploadImage");
var { isadminLoggedIn } = require("../utility/adminAuthToken");

const {
    createRoute,
    listRoute,
    updateRoute,
    deleteRoute,
  } = require("../controller/studentController");


router.post('/list', isadminLoggedIn, listRoute); 
router.post('/create', uploadImage.single("studentImage"), createRoute);  
router.post('/update/:id', uploadImage.single("studentImage"), updateRoute);
router.post('/delete/:id', deleteRoute);

module.exports = router;
  