const express = require('express')
const mongoose = require('mongoose')
const bodyparser = require('body-parser')
const path = require('path')
const { config } = require('process')
require('dotenv').config()

const usersRouter = require('./routes/user');
const adminRouter = require('./routes/admin');
const studentRouter = require('./routes/student');

const app = express();

//DataBase Configration 
require("./db/connection");

// app use
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

//routes
app.use('/admin', adminRouter);
app.use('/users', usersRouter);
app.use('/student', studentRouter);

// listening port
const PORT = process.env.APP_PORT || 3000;
app.listen(PORT, () => {
    console.log(`app is live at ${PORT}`);
});