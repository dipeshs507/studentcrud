var multer = require("multer");
var path = require("path");

const storage = multer.diskStorage(
    {
        destination: function (req, file, cb) {
            cb(null, './public/images');
        },
        filename: function (req, file, cb) {
            let modifiedname = `image-${Date.now() + path.extname(file.originalname)}`;
            cb(null, modifiedname);
        }
    });
const uploadFile = multer({ storage: storage })
module.exports = uploadFile