var JWT = require("jsonwebtoken");
var secretKey = process.env.SECRET_KEY

exports.isLoggedIn = (req, res, next) => {
    const token = req.header("auth-token");
    if (!token) return res.json({ message: "Access denied", status: 0 });
    try {
        const verified = JWT.verify(token, secretKey);
        req.user = verified.user;
        next();
    } catch (error) {
        let message = (!req.user) ?  "Unauthorized user" : error;
        res.json({ message: message, status: 0 })
    }
}