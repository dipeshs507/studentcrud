const mongoose = require("mongoose");
const userSchema = mongoose.Schema({
    userName: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    profileImage: {
        type: String,
        required: false,
        default: ""
    },
    // mobileNumber: {
    //     type: String,
    //     maxlength: 10,
    //     required: true,
    // },
    status: {
        type: Boolean,
        default: true
    }
},{ timestamps: true });

module.exports = mongoose.model("user", userSchema);

