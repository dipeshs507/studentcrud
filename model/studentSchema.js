var mongoose = require("mongoose");
var studentSchema = mongoose.Schema({ 
    name: {
        type: String,
        required: true
    },
    studentNumber: {
        type: String,
        required: true
    },
    classesEnrolled: {
        type: String,
        required:true
    },
    studentImage: {
        type: String,
        required: true
    },
}, { timestamps: true });

module.exports = mongoose.model("student", studentSchema);


