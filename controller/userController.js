var userSchema = require("../model/userSchema");
var bcrypt = require("bcryptjs");
var JWT = require("jsonwebtoken");

exports.userRegisterRoute = async(req, res, next) => {
    const { userName, email, password } = req.body;
    if (!userName) return res.json({ message: "userName must be required", status: 0 });
    if (!email) return res.json({ message: "Email must be required", status: 0 });
    if (!password) return res.json({ message: "Password must be required", status: 0 });

    const newUser = new userSchema({ userName, email, password });

    try {
        let user = await userSchema.findOne({ email });
        if (user) return res.json({ message: "Email already Exist!" });

        bcrypt.genSalt(10, (err, salt) => {
            if (err) return res.json({ message: "Something went wrong", status: 0 });

            bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) return res.json({ message: "Something went wrong", status: 0 });

                newUser.password = hash;
                newUser.save()
                    .then((data) => {
                        res.json({
                            message: "User registration successful.",
                            status: 1,
                            data
                        });
                    });
            });
        });
    } catch (err) {
        res.json({ message: "Internal server error!", status: 0, err })
    }
}

exports.userLoginRoute = async(req, res, next) => {
    const { email, password } = req.body;
    if (!email) return res.json({ message: "Email must be required", status: 0 });
    if (!password) return res.json({ message: "Password must be required", status: 0 });
    if (email && password) {
        let user = await userSchema.findOne({ email });
        if (!user) return res.json({ message: "Email not found", status: 0 });

        const match = await bcrypt.compare(password, user.password);
        if (match) {
            const token = await JWT.sign({ user }, process.env.SECRET_KEY, {
                expiresIn: "48h",
            });
            req.header("auth-token", token);
            res.json({ message: "User login successfully", status: 1, user, token });
        } else {
            res.json({ message: "Invalid username!", status: 0 });
        }
    }
};

exports.userProfileRoute = (req, res, next) => {
    userSchema.findOne({ email: req.user.email }).select("-password")
        .then((user) => {
            if (!user) return res.json({ message: "User not valid , try again", status: 0 })
            res.json({ message: "User profile details", status: 1, data: user })
        })
        .catch(() => {
            res.json({ message: "Internal server error!", status: 0 })
        })
}

exports.editProfileRoute = (req, res, next) => {
    const { userName, email, password } = req.body;
    const data = { userName, email, password }
    console.log(req.file);
    if (req.file) {
        data["profileImage"] = req.file.filename;
    }
    var editData = (data)
    userSchema.findOne({ email: req.user.email })
        .then((user) => {
            if (!user) return res.json({ message: "User not valid , try again", status: 0 })
            const id = user._id;
            try {
                userSchema.findByIdAndUpdate(id, { $set: editData }, { new: true, useFindAndModify: false })
                    .then((user) => res.json({ message: " Profile updated Successfully", status: 1, data: user }))
            } catch (err) {
                res.json({ message: "Internal server error!", status: 0 })
            }

        })
        .catch(() => {
            res.json({ message: "Internal server error!", status: 0 })
        })
}

exports.deleteUser = (req, res, next) => {
    const userId = req.body.userId;
    if (!userId) return res.json({ message: "Please select user to delete!", status: 0 });
    userSchema.findByIdAndDelete({ _id: userId },(err, result) => {
        if (err) return res.json({ message: "error in delete user!", status: 0 });
        res.json({ message: "Data deleted successful", status: 1 })
    });
}