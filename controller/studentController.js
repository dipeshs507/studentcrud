const studentSchema = require("../model/studentSchema");
const studentValidatorSchema = require("../validation/studentvalidation");

exports.createRoute = (req, res, next) => {
    let { name, studentNumber,  classesEnrolled } = req.body;
    const { error } = studentValidatorSchema.validate(req.body);
    if(error) return res.status(500).send({ error });
    console.log(req.file);
    if (!req.file) return res.json({ message: "Please select image!", status: 0 });
    
    let studentImage = req.file.filename;
    var studentData = new studentSchema({ name, studentNumber,  classesEnrolled, studentImage });
    studentData.save().then((student) => {
        res.json({
            message: "success",
            status: 1,
            data: student
        })
    })
    .catch(() => {
        res.json({ message: "Internal server error!", status: 0 })
    })
}

exports.listRoute = (req, res) => { 
    studentSchema.find()
    .then((students) => {                        
        res.json({
            message: "success",
            status: 1,
            data: students
        })
    })
    .catch((err) => {
        res.json({ message: "Something went wrong, please try again!", status: 0, err })
    })    
}


exports.updateRoute = (req, res) => {
    let studentID = req.params.id;
    if (!studentID) return res.json({ message: "studentID must be required", status: 0 }); 

    const { error } = studentValidatorSchema.validate(req.body);
    if(error) {
        return res.send({ message: error.message, status: 0 });
    }

    let { name, studentNumber,  classesEnrolled } = req.body;
    var id = { _id: studentID };
    var set = {  
        name, 
        studentNumber,  
        classesEnrolled,
        ...(req.file && { studentImage: req.file.filename })
    };
    studentSchema.findByIdAndUpdate(id, set, { useFindAndModify: false, new: true })
    .then((student) => {
        if(!student) return res.json({ message: "Something went wrong, please try again!", status: 0 })

        res.json({
            message: "success",
            status: 1,
            data: student
        })
    })
    .catch((err) => {
        res.json({ message: "Something went wrong, please try again!", status: 0, err })
    })
     
}

exports.deleteRoute = (req, res) => {
    let student_id = req.params.id;
    studentSchema.findById(student_id).then((student) => {
        if(!student) return res.json({ message: "student not found!", status: 0 })

        studentSchema.findByIdAndDelete(student_id)
            .then(() => {
                res.json({ message: "student deleted successfully!", status: 1 })            
            })
            .catch(err =>{
                res.json({ message: "Something went wrong, please try again!", status: 0, err })
            });
        });
}
  