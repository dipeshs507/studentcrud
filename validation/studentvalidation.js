const Joi =require("joi");

const studentValidatorSchema = Joi.object({
    name: Joi.string().required(),
    studentNumber: Joi.number().required(),
    classesEnrolled: Joi.string().required(),
});

module.exports = studentValidatorSchema;